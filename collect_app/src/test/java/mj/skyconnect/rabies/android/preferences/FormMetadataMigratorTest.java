package mj.skyconnect.rabies.android.preferences;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import mj.skyconnect.rabies.android.BuildConfig;
import mj.skyconnect.rabies.android.application.Collect;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/** Tests the FormMetadataFragment */
@Config(constants = BuildConfig.class)
@RunWith(RobolectricTestRunner.class)
public class FormMetadataMigratorTest {

    private SharedPreferences sharedPreferences;
    private final PrintStream printStream = System.out;

    /** The keys of preferences affected by the migration */
    private final List<String> affectedKeys = Arrays.asList(
            PreferenceKeys.KEY_METADATA_MIGRATED,
            PreferenceKeys.KEY_METADATA_USERNAME,
            PreferenceKeys.KEY_METADATA_PHONENUMBER,
            PreferenceKeys.KEY_METADATA_EMAIL,
            PreferenceKeys.KEY_USERNAME,
            PreferenceKeys.KEY_SELECTED_GOOGLE_ACCOUNT);

    /** The inputs to the migration */
    private final String[][] sourceKeyValuePairs = new String[][] {
            {PreferenceKeys.KEY_USERNAME,                  "a user"},
            {PreferenceKeys.KEY_SELECTED_GOOGLE_ACCOUNT,   "a Google email address"}
    };

    /** Changes to make to the metadata after the migration */
    private final String[][] modifiedMetadataValuePairs = new String[][] {
            {PreferenceKeys.KEY_METADATA_USERNAME,         "a user--changed"},
            {PreferenceKeys.KEY_METADATA_PHONENUMBER,      "a phone number--changed"},
            {PreferenceKeys.KEY_METADATA_EMAIL,            "an email--changed"},
    };

    @Before
    public void setUp() throws Exception {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Collect.getInstance());
    }

    @Test
    public void shouldMigrateDataCorrectly() {
        setPreferencesToPreMigrationValues();
        displayAffectedPreferences("Before calling migrate");

        FormMetadataMigrator.migrate(sharedPreferences);
        displayAffectedPreferences("After calling migrate");
        checkPostMigrationValues();

        setPreferencesToValues(modifiedMetadataValuePairs);
        displayAffectedPreferences("After changing metadata");
        FormMetadataMigrator.migrate(sharedPreferences);
        displayAffectedPreferences("After calling migrate again");
        ensureSecondMigrationCallPreservesMetadata();
    }

    private void displayAffectedPreferences(String message) {
        printStream.println("\n" + message);
        SortedMap<String, ?> allPrefs = new TreeMap<>(sharedPreferences.getAll());
        for (Map.Entry<String, ?> es : allPrefs.entrySet()) {
            if (affectedKeys.contains(es.getKey())) {
                printStream.format("%-25s %s\n", es.getKey(), es.getValue());
            }
        }
    }

    @SuppressLint("ApplySharedPref")
    private void setPreferencesToPreMigrationValues() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PreferenceKeys.KEY_METADATA_MIGRATED, false);
        editor.commit();

        setPreferencesToValues(sourceKeyValuePairs);
    }

    @SuppressLint("ApplySharedPref")
    private void setPreferencesToValues(String[][] valuePairs) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (String[] pair : valuePairs) {
            editor.putString(pair[0], pair[1]);
        }
        editor.commit();
    }

    private void checkPostMigrationValues() {
        assertTrue(sharedPreferences.getBoolean(PreferenceKeys.KEY_METADATA_MIGRATED, false));
        assertPrefsMatchValues(sourceKeyValuePairs);

        for (String[] pair : FormMetadataMigrator.sourceTargetValuePairs) {
            assertEquals(sharedPreferences.getString(pair[0], ""),
                    sharedPreferences.getString(pair[1], ""));
        }
    }

    private void ensureSecondMigrationCallPreservesMetadata() {
        assertPrefsMatchValues(modifiedMetadataValuePairs);
    }

    private void assertPrefsMatchValues(String[][] valuePairs) {
        for (String[] pair : valuePairs) {
            String prefValue = sharedPreferences.getString(pair[0], "");
            assertEquals(prefValue, pair[1]);
        }
    }
}
