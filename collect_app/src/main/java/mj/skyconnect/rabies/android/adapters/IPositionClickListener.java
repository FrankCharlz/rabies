package mj.skyconnect.rabies.android.adapters;

import mj.skyconnect.rabies.android.models.OdkHumanSimpleModel;

public interface IPositionClickListener {

    void clicked(OdkHumanSimpleModel o);
}
