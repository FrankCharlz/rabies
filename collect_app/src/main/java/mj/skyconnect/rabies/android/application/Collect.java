/*
 * Copyright (C) 2017 University of Washington
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package mj.skyconnect.rabies.android.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tumblr.remember.Remember;

import mj.skyconnect.rabies.android.API;
import mj.skyconnect.rabies.android.BuildConfig;
import mj.skyconnect.rabies.android.Constants;
import mj.skyconnect.rabies.android.database.ActivityLogger;
import mj.skyconnect.rabies.android.external.ExternalDataManager;
import mj.skyconnect.rabies.android.logic.FormController;
import mj.skyconnect.rabies.android.logic.PropertyManager;
import mj.skyconnect.rabies.android.preferences.AutoSendPreferenceMigrator;
import mj.skyconnect.rabies.android.utilities.LocaleHelper;
import mj.skyconnect.rabies.android.preferences.FormMetadataMigrator;
import mj.skyconnect.rabies.android.preferences.PreferenceKeys;
import mj.skyconnect.rabies.android.utilities.AgingCredentialsProvider;
import mj.skyconnect.rabies.android.utilities.AuthDialogUtility;
import mj.skyconnect.rabies.android.utilities.PRNGFixes;
import org.opendatakit.httpclientandroidlib.client.CookieStore;
import org.opendatakit.httpclientandroidlib.client.CredentialsProvider;
import org.opendatakit.httpclientandroidlib.client.protocol.HttpClientContext;
import org.opendatakit.httpclientandroidlib.impl.client.BasicCookieStore;
import org.opendatakit.httpclientandroidlib.protocol.BasicHttpContext;
import org.opendatakit.httpclientandroidlib.protocol.HttpContext;

import java.io.File;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * The Open Data Kit Collect application.
 *
 * @author carlhartung
 */
public class Collect extends Application {

    // Storage paths
    public static final String ODK_ROOT = Environment.getExternalStorageDirectory()
            + File.separator + "ses"; //todo: edited folder name here: 12-june-18 12:28
    public static final String FORMS_PATH = ODK_ROOT + File.separator + "forms";
    public static final String INSTANCES_PATH = ODK_ROOT + File.separator + "instances";
    public static final String CACHE_PATH = ODK_ROOT + File.separator + ".cache";
    public static final String METADATA_PATH = ODK_ROOT + File.separator + "metadata";
    public static final String TMPFILE_PATH = CACHE_PATH + File.separator + "tmp.jpg";
    public static final String TMPDRAWFILE_PATH = CACHE_PATH + File.separator + "tmpDraw.jpg";
    public static final String LOG_PATH = ODK_ROOT + File.separator + "log";
    public static final String DEFAULT_FONTSIZE = "21";
    public static final String OFFLINE_LAYERS = ODK_ROOT + File.separator + "layers";
    public static final String SETTINGS = ODK_ROOT + File.separator + "settings";
    private static final String TAG = Collect.class.getSimpleName();
    private static Collect singleton = null;

    static {
        PRNGFixes.apply();
    }

    // share all session cookies across all sessions...
    private CookieStore cookieStore = new BasicCookieStore();
    // retain credentials for 7 minutes...
    private CredentialsProvider credsProvider = new AgingCredentialsProvider(7 * 60 * 1000);
    private ActivityLogger activityLogger;
    private FormController formController = null;
    private ExternalDataManager externalDataManager;
    private Tracker tracker;

    public static String defaultSysLanguage;

    public static Collect getInstance() {
        return singleton;
    }

    public static int getQuestionFontsize() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Collect
                .getInstance());
        String questionFont = settings.getString(PreferenceKeys.KEY_FONT_SIZE,
                Collect.DEFAULT_FONTSIZE);
        return Integer.valueOf(questionFont);
    }

    /**
     * Creates required directories on the SDCard (or other external storage)
     *
     * @throws RuntimeException if there is no SDCard or the directory exists as a non directory
     */
    public static void createODKDirs() throws RuntimeException {
        String cardstatus = Environment.getExternalStorageState();
        if (!cardstatus.equals(Environment.MEDIA_MOUNTED)) {
            throw new RuntimeException(
                    Collect.getInstance().getString(mj.skyconnect.rabies.android.R.string.sdcard_unmounted, cardstatus));
        }

        String[] dirs = {
                ODK_ROOT, FORMS_PATH, INSTANCES_PATH, CACHE_PATH, METADATA_PATH, OFFLINE_LAYERS
        };

        for (String dirName : dirs) {
            File dir = new File(dirName);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    throw new RuntimeException("ODK reports :: Cannot create directory: "
                            + dirName);
                }
            } else {
                if (!dir.isDirectory()) {
                    throw new RuntimeException("ODK reports :: " + dirName
                            + " exists, but is not a directory");
                }
            }
        }
    }

    /**
     * Predicate that tests whether a directory path might refer to an
     * ODK Tables instance data directory (e.g., for media attachments).
     */
    public static boolean isODKTablesInstanceDataDirectory(File directory) {
        /*
         * Special check to prevent deletion of files that
         * could be in use by ODK Tables.
         */
        String dirPath = directory.getAbsolutePath();
        if (dirPath.startsWith(Collect.ODK_ROOT)) {
            dirPath = dirPath.substring(Collect.ODK_ROOT.length());
            String[] parts = dirPath.split(File.separator);
            // [appName, instances, tableId, instanceId ]
            if (parts.length == 4 && parts[1].equals("instances")) {
                return true;
            }
        }
        return false;
    }

    public ActivityLogger getActivityLogger() {
        return activityLogger;
    }

    public FormController getFormController() {
        return formController;
    }

    public void setFormController(FormController controller) {
        formController = controller;
    }

    public ExternalDataManager getExternalDataManager() {
        return externalDataManager;
    }

    public void setExternalDataManager(ExternalDataManager externalDataManager) {
        this.externalDataManager = externalDataManager;
    }

    public String getVersionedAppName() {
        String versionName = BuildConfig.VERSION_NAME;
        versionName = " " + versionName.replaceFirst("-", "\n");
        return getString(mj.skyconnect.rabies.android.R.string.app_name) + versionName;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getInstance()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo currentNetworkInfo = manager.getActiveNetworkInfo();
        return currentNetworkInfo != null && currentNetworkInfo.isConnected();
    }

    /*
        Adds support for multidex support library. For more info check out the link below,
        https://developer.android.com/studio/build/multidex.html
    */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * Construct and return a session context with shared cookieStore and credsProvider so a user
     * does not have to re-enter login information.
     */
    public synchronized HttpContext getHttpContext() {

        // context holds authentication state machine, so it cannot be
        // shared across independent activities.
        HttpContext localContext = new BasicHttpContext();

        localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
        localContext.setAttribute(HttpClientContext.CREDS_PROVIDER, credsProvider);

        return localContext;
    }

    public CredentialsProvider getCredentialsProvider() {
        return credsProvider;
    }

    public CookieStore getCookieStore() {
        return cookieStore;
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getInstance().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(View view) {
        view.requestFocus();
        InputMethodManager imm = (InputMethodManager) getInstance().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
    }

    @Override
    public void onCreate() {

        Remember.init(getApplicationContext(), this.getPackageName() + "_preferences");

        Stetho.initializeWithDefaults(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font.ttf")
                .setFontAttrId(mj.skyconnect.rabies.android.R.attr.fontPath)
                .build()
        );

        defaultSysLanguage = "sw"; //Locale.getDefault().getLanguage();
        new LocaleHelper().updateLocale(this);

        singleton = this;

        PreferenceManager.setDefaultValues(this, mj.skyconnect.rabies.android.R.xml.preferences, false);
        FormMetadataMigrator.migrate(PreferenceManager.getDefaultSharedPreferences(this));
        AutoSendPreferenceMigrator.migrate();
        super.onCreate();

        PropertyManager mgr = new PropertyManager(this);

        FormController.initializeJavaRosa(mgr);

        activityLogger = new ActivityLogger(
                mgr.getSingularProperty(PropertyManager.PROPMGR_DEVICE_ID));

        AuthDialogUtility.setWebCredentialsFromPreferences(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }

        initializeOkHttpClient();
    }

    private void initializeOkHttpClient() {
        Cache cache = new Cache(getCacheDir(), 1024 * 1024 * 20);
        //HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        //loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .followRedirects(false)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                //.addInterceptor(new DopeInterceptor())
                //.addInterceptor(loggingInterceptor)
                .build();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        log("CONFIGURATION CHANGED -------------->");
        log("LANGUAGE: "+PreferenceManager.getDefaultSharedPreferences(this)
                .getString(PreferenceKeys.KEY_APP_LANGUAGE, ""));

        super.onConfigurationChanged(newConfig);

        defaultSysLanguage = newConfig.locale.getLanguage();
        boolean isUsingSysLanguage = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(PreferenceKeys.KEY_APP_LANGUAGE, "").equals("");
        if (!isUsingSysLanguage) {
            new LocaleHelper().updateLocale(this);
        }
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */

    //todo: analytics
    public synchronized Tracker getDefaultTracker() {
        if (tracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            //tracker = analytics.newTracker(R.xml.global_tracker);
        }
        return tracker;
    }


    private static class CrashReportingTree extends Timber.Tree {
        @Override

        protected void log(int priority, String tag, String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
                return;
            }

            //todo: the will spy
//            FirebaseCrash.logcat(priority, tag, message);
//
//            if (t != null && priority == Log.ERROR) {
//                FirebaseCrash.report(t);
//            }

        }
    }

    //my additions

    private static OkHttpClient okHttpClient;
    private static Gson gson;
    private static API service;

    public static API API() {

        if (service == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL_V4)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            service = retrofit.create(API.class);
        }
        return service;

    }

    public static Gson getGson() {
        if (BuildConfig.DEBUG) {
            // do something for a debug build
            if (gson == null) gson = new GsonBuilder().setPrettyPrinting().create();
        } else {
            if (gson == null) gson = new GsonBuilder().create();
        }
        return gson;
    }

    public static void log(String msg) {
        Log.e("ses", msg);
    }

    public static void toast(Context context, String MSG) {
        Toast.makeText(context, MSG, Toast.LENGTH_SHORT).show();
    }


}
