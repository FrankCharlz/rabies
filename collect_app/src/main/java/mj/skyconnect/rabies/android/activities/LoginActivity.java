package mj.skyconnect.rabies.android.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import mj.skyconnect.rabies.android.GenesisActivity;
import mj.skyconnect.rabies.android.application.Collect;
import mj.skyconnect.rabies.android.models.User;
import mj.skyconnect.rabies.android.models.UserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends GenesisActivity implements Callback<UserResponse> {
    private ProgressDialog pdiag;

    private TextView.OnEditorActionListener mEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(final TextView view, int actionId, KeyEvent event) {

            //hide keyboard
            InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login(view);
                return true;
            }
            return false;

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(mj.skyconnect.rabies.android.R.layout.activity_login);

        Context context = this;
        Toolbar toolbar = (Toolbar) findViewById(mj.skyconnect.rabies.android.R.id.toolbar);
        setTitle("Login");
        setSupportActionBar(toolbar);

        //user experience (set default username as the last logged in username)
        //((EditText) findViewById(R.id.ed_auth_username)).setText(Remember.getString(User.PREFS_USER_NAME, ""));

        final EditText edPassword = ((EditText) findViewById(mj.skyconnect.rabies.android.R.id.ed_auth_password));
        edPassword.setOnEditorActionListener(mEditorActionListener);



    }

    public void login(View view) {
        EditText edUsername = (EditText) findViewById(mj.skyconnect.rabies.android.R.id.ed_auth_username);
        EditText edPassword = (EditText) findViewById(mj.skyconnect.rabies.android.R.id.ed_auth_password);

        String username = edUsername.getText().toString().trim();
        String password = edPassword.getText().toString().trim();

        if (username.isEmpty() || password.isEmpty()) {
            Collect.toast(getApplicationContext(), "All fields are mandatory");
        } else {
            authenticate(username, password);
        }

    }

    private void authenticate(String username, String password) {
        pdiag = new ProgressDialog(this);
        pdiag.setMessage("Logging in");
        pdiag.setCancelable(false);
        pdiag.show();

        Collect.API().login(username, password, "app").enqueue(this);
    }

    private void hidePdiag() {
        if (pdiag != null && pdiag.isShowing()) {
            pdiag.dismiss();
        }
    }

    private void doneLoggingInWithError(final String string) {
        hidePdiag();
        Collect.toast(getApplicationContext(), string);
    }

    @Override
    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
        if(response.isSuccessful()) {

            UserResponse user_response = response.body();

            if (user_response.success) {
                User user = user_response.user;
                user.setAsCurrentUser();
                user.saveToSharedPrefs();
                doneLoggingIn();
            } else {
                //success = false
                doneLoggingInWithError("Wrong user name or password");
            }

        } else {
            doneLoggingInWithError("Login was not successful, please try again");
        }
    }

    private void doneLoggingIn() {
        hidePdiag();
        finish();
        Intent intent = new Intent(this, MainMenu2Activity.class);
        LoginActivity.this.startActivity(intent);
    }


    @Override
    public void onFailure(retrofit2.Call<UserResponse> call, Throwable t) {
        hidePdiag();
        Collect.log("call failure : " + t.getLocalizedMessage());
        doneLoggingInWithError("Login failed:\n"+t.getLocalizedMessage());
    }


}
