package mj.skyconnect.rabies.android.models;

import mj.skyconnect.rabies.android.application.Collect;

/**
 * Created by Frank on 04-Aug-17.
 */
public class BaseResponse {

    public boolean success;

    @Override
    public String toString() {
        return Collect.getGson().toJson(this);
    }
}
