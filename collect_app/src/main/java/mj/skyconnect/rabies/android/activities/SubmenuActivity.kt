package mj.skyconnect.rabies.android.activities

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import mj.skyconnect.rabies.android.GenesisActivity
import mj.skyconnect.rabies.android.R
import mj.skyconnect.rabies.android.activities2.PendingCasesActivity
import mj.skyconnect.rabies.android.adapters.RecyclerAdapter
import mj.skyconnect.rabies.android.application.Collect
import mj.skyconnect.rabies.android.models.User
import mj.skyconnect.rabies.android.preferences.PreferenceKeys
import mj.skyconnect.rabies.android.utilities.ApplicationConstants
import mj.skyconnect.rabies.android.utilities.PlayServicesUtil

class SubmenuActivity : GenesisActivity() {


    private var standardMenuTexts = listOf(
            "Capture data",
            "Edit saved form",
            "Submit captured data",
            "Download updated forms",
            "Delete saved forms",
            "Pending cases"
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mj.skyconnect.rabies.android.R.layout.activity_submenu)

        prepareToolbar("Menu");

         var iconsNames = listOf(
                resources.getString(R.string.Capture_data),
                resources.getString(R.string.Edit_saved_form),
                resources.getString(R.string.Submit_captured_data),
                resources.getString(R.string.Download_updated_forms),
                resources.getString(R.string.Delete_saved_forms),
                resources.getString(R.string.Pending_cases)
        )


        if (User.getRole() == 1) {
            standardMenuTexts = standardMenuTexts.take(5);
            iconsNames = iconsNames.take(5)
        }




        val recyclerView = findViewById(mj.skyconnect.rabies.android.R.id.submenu_recycler) as RecyclerView

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager

        val recyclerAdapter = RecyclerAdapter(
                standardMenuTexts.toTypedArray(),
                iconsNames.toTypedArray(),
                RecyclerAdapter.ClickCallback { position -> handlePositions(position) }
        )

        recyclerView.adapter = recyclerAdapter
    }


    //  0 -- "Capture data",
    //  1 -- "Edit saved form",
    //  2 -- "Submit captured data",
    //  3 -- "Download updated forms",
    //  4 -- "Delete saved forms"
    //  4 -- "Pending cases"

    private fun handlePositions(position: Int) {

        when (position) {
            0 -> {
                val i = Intent(this, FormChooserList::class.java)
                startActivity(i)
            }

            1 -> {
                val i1 = Intent(applicationContext, InstanceChooserList::class.java)
                i1.putExtra(ApplicationConstants.BundleKeys.FORM_MODE,
                        ApplicationConstants.FormModes.EDIT_SAVED)
                startActivity(i1)
            }

            2 -> {
                val i2 = Intent(this, InstanceUploaderList::class.java)
                startActivity(i2)
            }

            3 -> downloadUpdatedForms()

            4 -> {
                Collect.getInstance().activityLogger
                        .logAction(this, "deleteSavedForms", "click")
                val i4 = Intent(applicationContext,
                        FileManagerTabs::class.java)
                startActivity(i4)
            }

            5 -> {
                val i5 = Intent(this, PendingCasesActivity::class.java)
                startActivity(i5)
            }

            else -> Log.d("MENU", "handlePositions: UNKNOWN option")
        }
    }


    private fun downloadUpdatedForms() {
        Collect.getInstance().activityLogger
                .logAction(this, "downloadBlankForms", "click")

        val sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this)
        val protocol = sharedPreferences.getString(
                PreferenceKeys.KEY_PROTOCOL, getString(mj.skyconnect.rabies.android.R.string.protocol_odk_default))

        var i: Intent? = null

        if (protocol!!.equals(getString(mj.skyconnect.rabies.android.R.string.protocol_google_sheets), ignoreCase = true)) {
            if (PlayServicesUtil.isGooglePlayServicesAvailable(this)) {
                i = Intent(applicationContext, GoogleDriveActivity::class.java)
            } else {
                PlayServicesUtil.showGooglePlayServicesAvailabilityErrorDialog(this)
                return
            }
        } else {
            i = Intent(applicationContext, FormDownloadList::class.java)
        }

        startActivity(i)
    }

    companion object {

        val TAG_SUB_MENU_TYPE = "submenu_type"
        val SUB_MENU_HC = 0x0f112
        val SUB_MENU_VET = 0x0f13
    }

}
