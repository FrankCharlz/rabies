package mj.skyconnect.rabies.android.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.tumblr.remember.Remember;

import mj.skyconnect.rabies.android.Constants;
import mj.skyconnect.rabies.android.GenesisActivity;
import mj.skyconnect.rabies.android.activities2.ProfileActivity;
import mj.skyconnect.rabies.android.application.Collect;
import mj.skyconnect.rabies.android.models.BaseResponse;
import mj.skyconnect.rabies.android.models.User;
import mj.skyconnect.rabies.android.preferences.AdminPreferencesActivity;
import mj.skyconnect.rabies.android.preferences.AutoSendPreferenceMigrator;
import mj.skyconnect.rabies.android.utilities.AuthDialogUtility;
import mj.skyconnect.rabies.android.utilities.SharedPreferencesUtils;
import mj.skyconnect.rabies.android.utilities.ToastUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MainMenu2Activity extends GenesisActivity {

    private static boolean EXIT = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(mj.skyconnect.rabies.android.R.layout.activity_main_menu2);

        //runs on the background
        sendTokenToServer();

        //initial things todo: gotta look much more closely here
        initialStuffs();

        //views
        initView();

    }

    private void initialStuffs() {
        // must be at the beginning of any activity that can be called from an external intent
        Log.e("rabies", "Starting up, creating directories");
        try {
            Collect.createODKDirs();
        } catch (RuntimeException e) {
            createErrorDialog(e.getMessage(), EXIT);
            return;
        }

        File f = new File(Collect.ODK_ROOT + "/collect.settings");
        File j = new File(Collect.ODK_ROOT + "/collect.settings.json");
        // Give JSON file preference
        if (j.exists()) {
            SharedPreferencesUtils sharedPrefs = new SharedPreferencesUtils();
            boolean success = sharedPrefs.loadSharedPreferencesFromJSONFile(j);
            if (success) {
                ToastUtils.showLongToast(mj.skyconnect.rabies.android.R.string.settings_successfully_loaded_file_notification);
                j.delete();

                // Delete settings file to prevent overwrite of settings from JSON file on next startup
                if (f.exists()) {
                    f.delete();
                }
            } else {
                ToastUtils.showLongToast(mj.skyconnect.rabies.android.R.string.corrupt_settings_file_notification);
            }
        } else if (f.exists()) {
            boolean success = loadSharedPreferencesFromFile(f);
            if (success) {
                ToastUtils.showLongToast(mj.skyconnect.rabies.android.R.string.settings_successfully_loaded_file_notification);
                f.delete();
            } else {
                ToastUtils.showLongToast(mj.skyconnect.rabies.android.R.string.corrupt_settings_file_notification);
            }
        }

    }

    private void sendTokenToServer() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Collect.log("sending FCM token to server: " + token);

        Collect.API().updateFCMToken(User.getId(), token).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    Collect.log("successfully sent token to server");
                    Remember.putBoolean(Constants.PREFS_SENT_GCM_TOKEN, true);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Collect.log("Could not update FCM token, network failure: "+ t.getLocalizedMessage());
            }
        });
    }

    private boolean loadSharedPreferencesFromFile(File src) {
        // this should probably be in a thread if it ever gets big
        boolean res = false;
        ObjectInputStream input = null;
        try {
            input = new ObjectInputStream(new FileInputStream(src));
            SharedPreferences.Editor prefEdit = PreferenceManager.getDefaultSharedPreferences(
                    this).edit();
            prefEdit.clear();
            // first object is preferences
            Map<String, ?> entries = (Map<String, ?>) input.readObject();

            AutoSendPreferenceMigrator.migrate(entries);

            for (Map.Entry<String, ?> entry : entries.entrySet()) {
                Object v = entry.getValue();
                String key = entry.getKey();

                if (v instanceof Boolean) {
                    prefEdit.putBoolean(key, (Boolean) v);
                } else if (v instanceof Float) {
                    prefEdit.putFloat(key, (Float) v);
                } else if (v instanceof Integer) {
                    prefEdit.putInt(key, (Integer) v);
                } else if (v instanceof Long) {
                    prefEdit.putLong(key, (Long) v);
                } else if (v instanceof String) {
                    prefEdit.putString(key, ((String) v));
                }
            }
            prefEdit.apply();
            AuthDialogUtility.setWebCredentialsFromPreferences(this);

            // second object is admin options
            SharedPreferences.Editor adminEdit = getSharedPreferences(AdminPreferencesActivity.ADMIN_PREFERENCES,
                    0).edit();
            adminEdit.clear();
            // first object is preferences
            Map<String, ?> adminEntries = (Map<String, ?>) input.readObject();
            for (Map.Entry<String, ?> entry : adminEntries.entrySet()) {
                Object v = entry.getValue();
                String key = entry.getKey();

                if (v instanceof Boolean) {
                    adminEdit.putBoolean(key, (Boolean) v);
                } else if (v instanceof Float) {
                    adminEdit.putFloat(key, (Float) v);
                } else if (v instanceof Integer) {
                    adminEdit.putInt(key, (Integer) v);
                } else if (v instanceof Long) {
                    adminEdit.putLong(key, (Long) v);
                } else if (v instanceof String) {
                    adminEdit.putString(key, ((String) v));
                }
            }
            adminEdit.apply();

            res = true;
        } catch (IOException | ClassNotFoundException e) {
            Timber.e(e, "Exception while loading preferences from file due to : %s ", e.getMessage());
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException ex) {
                Timber.e(ex, "Exception thrown while closing an input stream due to: %s ", ex.getMessage());
            }
        }
        return res;
    }

    private void initView() {
        TextView tvOwnerFacility = (TextView) findViewById(mj.skyconnect.rabies.android.R.id.tv_device_owner_facility);
        tvOwnerFacility.setText(User.getFacilityName());


        TextView tvOwnerName = (TextView) findViewById(mj.skyconnect.rabies.android.R.id.tv_device_owner_name);
        tvOwnerName.setText(User.getOwnerName());

    }

    public void openHCSubmenu(View view) {
        Log.e("MJ", "openHCSubmenu: user role: " + User.getRole());
        if (User.getRole() != 1) {
            Collect.toast(this, "You can not access this portal");
            return;
        }

        Intent intent = new Intent(this, SubmenuActivity.class);
        intent.putExtra(SubmenuActivity.Companion.getTAG_SUB_MENU_TYPE(), SubmenuActivity.Companion.getSUB_MENU_HC());
        startActivity(intent);
    }

    public void openLFOSubmenu(View view) {
        Log.e("MJ", "openHCSubmenu: user role: " + User.getRole());
        if (User.getRole() != 2) {
            Collect.toast(this, "You can not access this portal");
            return;
        }

        Intent intent = new Intent(this, SubmenuActivity.class);
        intent.putExtra(SubmenuActivity.Companion.getTAG_SUB_MENU_TYPE(), SubmenuActivity.Companion.getSUB_MENU_VET());
        startActivity(intent);
    }

    public void openProfile(View view) {
        startActivity(new Intent(this, ProfileActivity.class));
    }

    public void openSettings(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void logOut(final View view) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        User.logout();
                        openLoginActivity();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        new AlertDialog
                .Builder(this)
                .setMessage(getString(mj.skyconnect.rabies.android.R.string.logout_confirmation_alert))
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show();
    }

    private void openLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish(); //always finish when going to login activity
    }

    public void openHelp(View view) {

        final String phoneNumber = Remember.getString(Constants.PREFS_HELP_DESK_NO, "+255624033062");
        final String emailAddress = Remember.getString(Constants.PREFS_HELP_DESK_EMAIL, "symonnkumbugwa@gmail.com");
        String[] menuArray = new String[] {"Call help desk", "Send an email"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("For immediate support:")
                .setItems(menuArray, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        switch (which) {
                            case 0:
                                Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null));
                                startActivity(dialIntent);
                                break;

                            case 1:
                                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",emailAddress, null));
                                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "USER NEEDS ASSISTANCE");
                                String emailTemplate = " needs your assistance.\n\n***Express your issue below***\n\n";
                                emailIntent.putExtra(Intent.EXTRA_TEXT, User.getOwnerName() + emailTemplate);
                                startActivity(Intent.createChooser(emailIntent, "Send email to support:"));
                                break;

                            default: break;
                        }

                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    private void createErrorDialog(String errorMsg, final boolean shouldExit) {
        AlertDialog mAlertDialog = new AlertDialog.Builder(this).create();
        mAlertDialog.setIcon(android.R.drawable.ic_dialog_info);
        mAlertDialog.setMessage(errorMsg);
        DialogInterface.OnClickListener errorListener = new DialogInterface.OnClickListener() {
            /*
             * (non-Javadoc)
             * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
             */
            @Override
            public void onClick(DialogInterface dialog, int i) {
                switch (i) {
                    case DialogInterface.BUTTON1:
                        if (shouldExit) {
                            finish();
                        }
                        break;
                }
            }
        };
        mAlertDialog.setCancelable(false);
        //mAlertDialog.setButton(getString(R.string.ok), errorListener); //todo: here here...
        mAlertDialog.show();
    }

    public void openCommunitySubmenu(View view) {
        if (User.getId() != 3) {
            Collect.toast(this, "You can not access this portal");
            return;
        }

    }
}
