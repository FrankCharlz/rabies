package mj.skyconnect.rabies.android.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OdkHumanSimpleModel extends BaseResponse implements Serializable {

        @SerializedName("SURNAME")
        @Expose
        public String sURNAME;

        @SerializedName("OTHER_NAMES")
        @Expose
        public String oTHERNAMES;

        @SerializedName("POPULAR_NAME")
        @Expose
        public String popularName;

        @SerializedName("USERNAME")
        @Expose
        public String username;

        @SerializedName("_URI")
        @Expose
        public String uri;

        @SerializedName("_CREATION_DATE")
        @Expose
        public String date;

        @SerializedName("sender")
        @Expose
        public String sender;

        @SerializedName("facilityName")
        @Expose
        public String facilityName;

        @SerializedName("title")
        @Expose
        public String title;

}