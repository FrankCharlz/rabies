package mj.skyconnect.rabies.android.models;

import java.util.List;

/**
 * Created by Frank on 5/9/2016.
 *
 */

public class Region {
    public int id;
    public String name;
    public List<District> districts;
}