package mj.skyconnect.rabies.android.models;

/**
 * Created by Frank on 8/25/16.
 *
 */
public class ReviewerData {

    public String comment;
    public String assessment;
}
