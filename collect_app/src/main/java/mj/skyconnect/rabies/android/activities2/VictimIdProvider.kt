package mj.skyconnect.rabies.android.activities2

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Button
import android.widget.Toast
import mj.skyconnect.rabies.android.GenesisActivity
import mj.skyconnect.rabies.android.R
import mj.skyconnect.rabies.android.adapters.CasesListAdapter
import mj.skyconnect.rabies.android.adapters.IPositionClickListener
import mj.skyconnect.rabies.android.application.Collect
import mj.skyconnect.rabies.android.models.OdkHumanSimpleModel
import mj.skyconnect.rabies.android.models.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


@SuppressLint("SetTextI18n")
class VictimIdProvider : GenesisActivity() {

    private val cases = ArrayList<OdkHumanSimpleModel>();
    private lateinit var adapter: CasesListAdapter
    private lateinit var button: Button

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_victim_id_provider)


        val o =  OdkHumanSimpleModel()
        o.title = "ID NOT SPECIFIED"
        o.uri = "ID_NOT_SPECIFIED"
        o.sender = "None"
        o.date = "0000-00-00 00:00"
        cases.add(o)

        button = findViewById(R.id.button2) as Button
        button.text = "Loading..."
        button.isEnabled = false

        button.setOnClickListener {
            val intent = Intent()
            intent.putExtra(VICTIM_ID, "ID_NOT_SPECIFIED")
            setResult(RESULT_OK, intent)
            super.finish()
        }

        val recyclerView = findViewById(R.id.recycler) as RecyclerView
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager

        adapter = CasesListAdapter(
                cases,
                IPositionClickListener {

                    val victimId = it.uri
                    val intent = Intent()
                    intent.putExtra(VICTIM_ID, victimId)
                    setResult(RESULT_OK, intent)
                    finish()

                }
        )

        recyclerView.adapter = adapter

        fetchCases()

    }

    private fun fetchCases() {
        Collect.API().getAllHumanCases(User.getId()).enqueue(object : Callback<List<OdkHumanSimpleModel>> {
            override fun onResponse(call: Call<List<OdkHumanSimpleModel>>, response: Response<List<OdkHumanSimpleModel>>) {
                if (response.isSuccessful) {
                    Toast.makeText(applicationContext, "Request successful", Toast.LENGTH_LONG).show()
                    response.body()?.let { cases.addAll(it) }

                    adapter.refreshData(cases)
                    button.text = "Done"
                    button.isEnabled = true


                } else {
                    Toast.makeText(applicationContext, "Request failed", Toast.LENGTH_LONG).show()

                    button.text = "Done with Error"
                    button.isEnabled = true
                }
            }

            override fun onFailure(call: Call<List<OdkHumanSimpleModel>>, t: Throwable) {
                Log.e("Victim", "onFailure: ", t)
                button.text = "Done with Error (2)"
                button.isEnabled = true
            }
        })
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent()
        intent.putExtra(VICTIM_ID, "ID_NOT_SPECIFIED")
        setResult(RESULT_OK, intent)
        finish()
    }

    companion object {
        val VICTIM_ID : String = "victim_id"
    }
}
