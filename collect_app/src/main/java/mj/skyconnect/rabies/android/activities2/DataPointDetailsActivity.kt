package mj.skyconnect.rabies.android.activities2

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient

import java.net.URI

import mj.skyconnect.rabies.android.GenesisActivity
import mj.skyconnect.rabies.android.R
import mj.skyconnect.rabies.android.models.User
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class DataPointDetailsActivity : GenesisActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_datapoint_details)

        val uri = intent.getStringExtra(_URI)

        prepareToolbar(uri)

        val webView = findViewById(R.id.webview) as WebView
        webView.settings.javaScriptEnabled = true
        //Only disabled the horizontal scrolling:
        webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
        //to prevent opening in chrome
        webView.webViewClient = WebViewClient()
        webView.loadUrl(BASE_URL + uri)

    }

    companion object {
        public val _URI = "URI"
        private val BASE_URL = "http://rabies.esurveillance.or.tz/data/view-mobile/"
    }

}