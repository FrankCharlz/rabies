package mj.skyconnect.rabies.android;


import java.util.List;

import mj.skyconnect.rabies.android.models.BaseResponse;
import mj.skyconnect.rabies.android.models.OdkHumanModel;
import mj.skyconnect.rabies.android.models.OdkHumanSimpleModel;
import mj.skyconnect.rabies.android.models.UserResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Frank on 04-Aug-17.
 */

public interface API {

    @FormUrlEncoded
    @POST("login")
    Call<UserResponse> login(
            @Field("username") String username,
            @Field("password") String password,
            @Field("source") String source
    );

    @FormUrlEncoded
    @POST("updateFcmToken")
    Call<BaseResponse> updateFCMToken(
            @Field("userId") int userId,
            @Field("newToken") String newToken
    );

    @FormUrlEncoded
    @POST("instanceUploaded")
    Call<BaseResponse> instanceUploaded(
            @Field("userId") int userId,
            @Field("instanceId") String meta
    );

    @GET("cases/hrc/{user}")
    Call<List<OdkHumanSimpleModel>> getHighRiskHumanCases(@Path("user") int vet);

    @GET("cases/human/all/{user}")
    Call<List<OdkHumanSimpleModel>> getAllHumanCases(@Path("user") int vet);


}
