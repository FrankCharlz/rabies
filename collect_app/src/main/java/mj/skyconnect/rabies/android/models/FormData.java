
package mj.skyconnect.rabies.android.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import mj.skyconnect.rabies.android.application.Collect;

import java.util.LinkedHashMap;

public class FormData {

    @SerializedName("form_name")
    @Expose
    public String formName;

    @SerializedName("form_id")
    @Expose
    public String formId;


    @SerializedName("patient_id")
    @Expose
    public String patientId;

    @SerializedName("ccs_providers_full_name")
    @Expose
    public String ccsProvidersFullName;

    @SerializedName("ccs_providers_phone_no")
    @Expose
    public String ccsProvidersPhoneNo;

    @SerializedName("screen_date")
    @Expose
    public String screenDate;

    @SerializedName("fname")
    @Expose
    public String fname;

    @SerializedName("mname")
    @Expose
    public String mname;

    @SerializedName("lname")
    @Expose
    public String lname;

    @SerializedName("region")
    @Expose
    public String region;

    @SerializedName("district")
    @Expose
    public String district;

    @SerializedName("village_street")
    @Expose
    public String villageStreet;

    @SerializedName("phone_number")
    @Expose
    public String phoneNumber;

    @SerializedName("age")
    @Expose
    public String age;
    @SerializedName("parity")
    @Expose
    public String parity;

    @SerializedName("ccs_facility_no")
    @Expose
    public String ccsFacilityNo;

    @SerializedName("ccs_client_no")
    @Expose
    public String ccsClientNo;

    @SerializedName("ctc_no")
    @Expose
    public String ctcNo;


    @SerializedName("ccs_no")
    @Expose
    public String ccsNo;

    @SerializedName("patient_received_pitc")
    @Expose
    public String patientReceivedPitc;

    @SerializedName("rd_gender")
    @Expose
    public String rdGender;

    @SerializedName("hiv_status")
    @Expose
    public String hivStatus;

    @SerializedName("assessment")
    @Expose
    public String assessment;
    @SerializedName("visit_status")
    @Expose
    public String visitStatus;

    @SerializedName("photo_1")
    @Expose
    public String photo1;

    @SerializedName("photo_2")
    @Expose
    public String photo2;

    @SerializedName("photo_3")
    @Expose
    public String photo3;


    @Override
    public String toString() {
        return Collect.getGson().toJson(this);
    }

    private String getHIVStatus() {
        if (hivStatus == null) return "Null";
        return hivStatus;
    }

    private boolean hivPositive() {
        return hivStatus.equalsIgnoreCase("positive");
    }

    private String getFacilityNumber() {
        //facility number algorithm
        //Kagera, Muleba DC, Kaigara, 0028-0003-0102
        //fn comes as above, so only take the last digits
        //must have comma before
        String[] fn_chunks;
        String fn = "null-null-null";
        if (ccsFacilityNo != null) {
            fn_chunks = ccsFacilityNo.split(",");
        } else  {
            fn_chunks = new String[] {""};
        }
        if (fn_chunks.length > 0) fn = fn_chunks[fn_chunks.length - 1].trim();
        return  fn;
    }
    
    public LinkedHashMap<String, String> toHashMap() {
        LinkedHashMap<String, String> map = new LinkedHashMap<>(7);
        map.put("Patient ID", this.patientId);

        map.put("Age", this.age);
        map.put("Parity", this.parity);
        map.put("HIV status", this.getHIVStatus());

        if (this.hivPositive()) {
            map.put("CTC number", this.getFacilityNumber() + this.ctcNo);
        } else {
            map.put("CTC number", this.getFacilityNumber() + this.ccsNo);
        }

        map.put("Visit status", this.visitStatus);
        map.put("Phone number", this.phoneNumber);

        return map;
    }
}
