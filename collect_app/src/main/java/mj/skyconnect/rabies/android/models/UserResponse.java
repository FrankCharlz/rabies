package mj.skyconnect.rabies.android.models;

/**
 * Created by Frank on 22-Aug-17.
 */

public class UserResponse extends BaseResponse{
    public String token;
    public String message;
    public User user;

}
