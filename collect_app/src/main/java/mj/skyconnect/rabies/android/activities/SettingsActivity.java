package mj.skyconnect.rabies.android.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;


import com.tumblr.remember.Remember;

import mj.skyconnect.rabies.android.BuildConfig;
import mj.skyconnect.rabies.android.Constants;
import mj.skyconnect.rabies.android.R;
import mj.skyconnect.rabies.android.application.Collect;
import mj.skyconnect.rabies.android.preferences.PreferenceKeys;
import mj.skyconnect.rabies.android.preferences.PreferencesActivity;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingsActivity extends AppCompatActivity {


    private Activity activity;
    private View.OnClickListener tvVersionClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    };
    private TextView tvLang;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        activity = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle("Settings");
        setSupportActionBar(toolbar);

        int versionCodeApp = BuildConfig.VERSION_CODE;
        String version = BuildConfig.VERSION_NAME;

        tvLang = (TextView) findViewById(R.id.lang_settings);
        tvLang.setText(Remember.getString(Constants.PREFS_LANG, "English"));

        TextView tvVersion = (TextView) findViewById(R.id.version_settings);
        if (tvVersion != null) {
            tvVersion.setText( String.format(Locale.US, "%s, %d",version, versionCodeApp));
        }

        View btnOpenSourceLicense = findViewById(R.id.open_source_licenses);
        btnOpenSourceLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://cervical.esurveillance.or.tz/open_source_libs.php";

                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
                builder.setShowTitle(true);
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(activity, Uri.parse(url));
            }
        });

        View btnDashboard = findViewById(R.id.btn_dashboard);
        btnDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://dashboards.esurveillance.or.tz/rabies";

                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
                builder.setShowTitle(true);
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(activity, Uri.parse(url));
            }
        });

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase)); //for fonts
    }

    public void selectLanguage(View view) {
        final String[] languages = new String[] {"Swahili", "English", "Spanish", "French"};
        final String[] languageCodes = new String[] {"sw", "en", "es", "fr"};

        final int[] selectedItem = {0};

        new AlertDialog
                .Builder(view.getContext())
                .setTitle("Choose language")
                .setSingleChoiceItems(languages, selectedItem[0], new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedItem[0] = which;
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK, so save the mSelectedItems results somewhere
                        // or return them to the component that opened the dialog
                        Collect.toast(getApplicationContext(), languages[selectedItem[0]]+" selected");

                        Remember.putString(Constants.PREFS_LANG, languages[selectedItem[0]]);
                        PreferenceManager
                                .getDefaultSharedPreferences(SettingsActivity.this)
                                .edit()
                                .putString(PreferenceKeys.KEY_APP_LANGUAGE, languageCodes[selectedItem[0]])
                                .apply();
                        
                        tvLang.setText(languages[selectedItem[0]]);

                        String languageToLoad  = languageCodes[selectedItem[0]];
                        Locale locale = new Locale(languageToLoad);
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        activity.getBaseContext()
                                .getResources()
                                .updateConfiguration(
                                        config,
                                        getBaseContext().getResources().getDisplayMetrics()
                                );
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .create()
                .show();

    }

    public void generalSettings(View view) {
        Intent i = new Intent(this, PreferencesActivity.class);
        startActivity(i);
    }
}
