package mj.skyconnect.rabies.android;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Frank on 19-Jun-18.
 *
 */

public class FontManager {

    public static final String DEFAULT = "font.ttf";
    public static final String ION_ICONS = "ionicons.ttf";

    private static Map<String, Typeface> cachedFonts = new HashMap<String, Typeface>();

    public static Typeface getTypeface(Context context, String font) {

        if (!cachedFonts.containsKey(font)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), font);
            cachedFonts.put(font, tf);
        }

        return cachedFonts.get(font);

    }

    public static Typeface getTypeface(Context context) {
        return getTypeface(context, DEFAULT);
    }

    public static void setTypeface(TextView textView, String fontName) {
        textView.setTypeface(getTypeface(textView.getContext(), fontName));
    }
}
