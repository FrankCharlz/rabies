package mj.skyconnect.rabies.android.activities2;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import mj.skyconnect.rabies.android.Constants;
import mj.skyconnect.rabies.android.R;
import mj.skyconnect.rabies.android.models.User;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        WebView webView = (WebView) findViewById(R.id.webview_profile);
        webView.getSettings().setJavaScriptEnabled(true);

        //Only disabled the horizontal scrolling:
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        //to prevent opening in chrome
        webView.setWebViewClient(new WebViewClient());

        String url = "http://rabies.esurveillance.or.tz/users/"+User.getId();

        Log.i("MJ", "onCreate: " + url);

        webView.loadUrl(url);

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        //for fonts
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
