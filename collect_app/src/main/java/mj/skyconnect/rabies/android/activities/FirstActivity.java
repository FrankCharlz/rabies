package mj.skyconnect.rabies.android.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import mj.skyconnect.rabies.android.R;

public class FirstActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST_MULTIPLE = 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

    }


    private void checkPermissions() {

        String permissions[] = new String[] {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_PHONE_STATE
        };

        ArrayList<String> ungrantedPermissions = new ArrayList<>();

        for (String permission : permissions) {

            if (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                ungrantedPermissions.add(permission);
            } else {
                // Permission has already been granted
                Log.i("MJ", "checkPermissions: Permission already granted " + permission);
            }
        }

        if (ungrantedPermissions.size() > 0) {
            doRequestPermissions(ungrantedPermissions);
        } else {
            doStartSplashActivity();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPermissions();
    }

    private void doStartSplashActivity() {
        startActivity(new Intent(FirstActivity.this, SplashScreenActivity.class));
        finish();
    }

    private void doRequestPermissions(ArrayList<String> ungrantedPermissions) {
        //todo: show rationale if possible

        for (String permission: ungrantedPermissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {

            }
        }

        String p[] = new String[ungrantedPermissions.size()];
        for (int i = 0; i < p.length; i++) {
            p[i] = ungrantedPermissions.get(i);
        }

        ActivityCompat.requestPermissions(this, p, PERMISSIONS_REQUEST_MULTIPLE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_MULTIPLE:
                boolean foo = false;
                for (int result: grantResults) {
                    foo = foo || (result == PackageManager.PERMISSION_DENIED);
                }

                if (foo)  {
                    //exit even if only a single permission is denied
                    Snackbar
                            .make(
                                    findViewById(android.R.id.content),
                                    getString(R.string.bel_permission_rationale),
                                    Snackbar.LENGTH_INDEFINITE
                            )
                            .setAction("Settings", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                }
                            })
                            .show();

                } else {
                    //no single permission denied
                    doStartSplashActivity();
                    break;
                }


            default: break;

        }
    }



}
