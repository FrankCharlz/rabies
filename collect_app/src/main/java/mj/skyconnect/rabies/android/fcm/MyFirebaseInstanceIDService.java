/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mj.skyconnect.rabies.android.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.tumblr.remember.Remember;


import mj.skyconnect.rabies.android.Constants;
import mj.skyconnect.rabies.android.application.Collect;
import mj.skyconnect.rabies.android.models.BaseResponse;
import mj.skyconnect.rabies.android.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */

    private final String TAG = "FIREBASE:"+MyFirebaseInstanceIDService.class;
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Collect.log("Refreshed token: " + refreshedToken);

        Collect.API().updateFCMToken(User.getId(), refreshedToken).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    Log.i(TAG, "refreshed token success");
                    Remember.putBoolean(Constants.PREFS_SENT_GCM_TOKEN, true);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Collect.log("Could not update FCM token, network failure: "+ t.getLocalizedMessage());
            }
        });

    }



}