package mj.skyconnect.rabies.android.activities2

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import mj.skyconnect.rabies.android.GenesisActivity
import mj.skyconnect.rabies.android.R
import mj.skyconnect.rabies.android.adapters.CasesListAdapter
import mj.skyconnect.rabies.android.adapters.IPositionClickListener
import mj.skyconnect.rabies.android.application.Collect
import mj.skyconnect.rabies.android.models.OdkHumanSimpleModel
import mj.skyconnect.rabies.android.models.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class PendingCasesActivity : GenesisActivity() {
    private val cases = ArrayList<OdkHumanSimpleModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pending_cases)

        prepareToolbar("Pending cases");

        val recyclerView = findViewById(R.id.recycler) as RecyclerView

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager

        val adapter = CasesListAdapter(
                cases,
                IPositionClickListener { o ->
                    val intent = Intent(this@PendingCasesActivity, DataPointDetailsActivity::class.java)
                    intent.putExtra(DataPointDetailsActivity._URI, o.uri)
                    startActivity(intent)
                }
        )

        recyclerView.adapter = adapter

        Collect.API().getHighRiskHumanCases(User.getId()).enqueue(object : Callback<List<OdkHumanSimpleModel>> {
            override fun onResponse(call: Call<List<OdkHumanSimpleModel>>, response: Response<List<OdkHumanSimpleModel>>) =
                    if (response.isSuccessful) {
                        Toast.makeText(applicationContext, "Request successful", Toast.LENGTH_LONG).show()

                        response.body()?.let { cases.addAll(it) }
                        Log.i(TAG, "onResponse: cases: " + cases.size)
                        adapter.refreshData(cases)

                    } else {
                        Toast.makeText(applicationContext, "Request failed", Toast.LENGTH_LONG).show()
                    }

            override fun onFailure(call: Call<List<OdkHumanSimpleModel>>, t: Throwable) {
                Log.e(TAG, "onFailure: ", t)
            }
        })

    }

}
