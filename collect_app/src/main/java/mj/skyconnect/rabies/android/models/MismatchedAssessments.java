package mj.skyconnect.rabies.android.models;

/**
 * Created by Frank on 05-Sep-17.
 */

public class MismatchedAssessments {

    public int case_id, creator_id, reviewer_id, assessment_mismatch;
    public String date_created;
    public ReviewerData data;

}
