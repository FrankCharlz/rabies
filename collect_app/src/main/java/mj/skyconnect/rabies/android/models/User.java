package mj.skyconnect.rabies.android.models;


import com.tumblr.remember.Remember;

import mj.skyconnect.rabies.android.application.Collect;
import mj.skyconnect.rabies.android.preferences.PreferenceKeys;

import java.util.List;

/**
 * Created by Frank on 09-Aug-17.
 */

public class User {

    public static final int ROLE_REVIEWER = 2;
    public static final int ROLE_PROVIDER = 1;
    private static final String PREFS_USER_ID = "__user_id";
    private static final String PREFS_USER_NAME = "__username";
    private static final String PREFS_USER_ROLE = "__user_role";
    private static final String PREFS_DEVICE_OWNER_NAME = "__owner_name";
    private static final String PREFS_DEVICE_OWNER_FACILITY = "__owner_facility";
    private static final String PREFS_USER_GRADUATED = "__graduate";
    private static final String AUTH_TOKEN = "__auth_token";
    private static final String PREFS_LAST_LAUNCH_TIME = "__last_launch_timestamp";
    private static final String PREFS_FACILITY_ID = "__fac_id__";
    private static final String PREFS_FORM_IDS = "__form_ids_0";
    private static final String PREFS_CURRENT_USER_JSON = "__current_user_json";

    private static User sCurrentUser;

    public int id, role, status, facility_id;
    public String username, fullname, phone, email;

    private List<Facility> facilities;
    private List<District> districts;
    private List<String> formIds;

    //todo: login pro..

    /***
     * when user logs in.. statically save crucial data//
     * avoid shared pref to save memory..
     * u are drunk now, do this tomorrow when u r sober fuuuuck...
     *
     * @return null
     */


    //BAO
    public static int getId() {
        return Remember.getInt(PREFS_USER_ID, 0);
    }

    public static int getRole() {
        return Remember.getInt(PREFS_USER_ROLE, 0);
    }

    public static boolean isGraduate() {
        return Remember.getBoolean(PREFS_USER_GRADUATED, false);
    }

    public static boolean isLoggedIn() {
        //if ID is greater than 0 //// TODO: 07-Sep-17 imarisha
        return  Remember.getInt(PREFS_USER_ID, 0) > 0;
    }

    public static String getOwnerName() {
        return Remember.getString(PREFS_DEVICE_OWNER_NAME, "");
    }

    public static String getFacilityName() {
        return Remember.getString(User.PREFS_DEVICE_OWNER_FACILITY, "-");
    }


    public static User getCurrentUser() {
        if (sCurrentUser == null) {
            String currentUserJson = Remember.getString(PREFS_CURRENT_USER_JSON, null);
            sCurrentUser = Collect.getGson().fromJson(currentUserJson, User.class);
        }
        return sCurrentUser;
    }

    public void saveToSharedPrefs() {

        String me = Collect.getGson().toJson(this);

        Remember.putString(PREFS_CURRENT_USER_JSON, me);
        Collect.log("saving user to shared preference");
        Remember.putInt(PREFS_USER_ID, id);
        Remember.putInt(PREFS_USER_ROLE, role);
        Remember.putString(PREFS_USER_NAME, username);
        Remember.putString(PREFS_DEVICE_OWNER_NAME, fullname);

        //important -- this links the form instance to the submitter
        //todo: save this separately using default sp
        Remember.putString(PreferenceKeys.KEY_METADATA_USERNAME, id+"");

        if (this.formIds != null) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < this.formIds.size(); i++) {
                sb.append(this.formIds.get(i)).append(",");
            }
            Remember.putString(PREFS_FORM_IDS, sb.toString());
            Collect.log("updating user forms in shared preference");
        } else {
            Collect.log("forms is null");
        }

        if (this.facilities.size() > 0 && this.role == 1) {
            Remember.putString(PREFS_DEVICE_OWNER_FACILITY, this.facilities.get(0).name);
        }

        if (this.districts.size() > 0 && this.role == 2) {
            Remember.putString(PREFS_DEVICE_OWNER_FACILITY, this.districts.get(0).name);
        }

    }

    public static boolean expired() {
        final long time_window = 60 * 60 * 1000L; //1 hour
        long lastLaunchTime = Remember.getLong(PREFS_LAST_LAUNCH_TIME, 0L);
        long currentTime = System.currentTimeMillis();

        long diff = currentTime - lastLaunchTime;

        Remember.putLong(PREFS_LAST_LAUNCH_TIME, currentTime);

        return lastLaunchTime > 0 && diff > time_window;
    }

    public static void logout() {

        String itemsToBeCleared[] = new String[] {
                PREFS_DEVICE_OWNER_FACILITY,
                PREFS_DEVICE_OWNER_NAME,
                PREFS_USER_GRADUATED,
                PREFS_USER_ID,
                PREFS_USER_ROLE,
                AUTH_TOKEN,
                PREFS_FORM_IDS,
                PreferenceKeys.KEY_METADATA_USERNAME
        };

        for (String item : itemsToBeCleared) Remember.remove(item);
    }

    public static String[] getFormIds() {
        String ids = Remember.getString(PREFS_FORM_IDS, "");
        Collect.log(ids);

        if (ids.length() == 0)
            return  new String[]{};
        else
            return  ids.split(",");
    }

    public void setAsCurrentUser() {
        //todo: implement
    }
}
