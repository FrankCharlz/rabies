package mj.skyconnect.rabies.android.models;

/**
 * Created by Frank on 05-Sep-17.
 */

public class ReviewedCaseData {

    public FormData data;

    public int case_id, creator_id, status, export, rid, mismatch;

    public String date_data_created, rdate;

    public  ReviewerData rdata;
}
