/*
 * Copyright (C) 2017 Shobhit
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package mj.skyconnect.rabies.android.preferences;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import org.javarosa.core.services.IPropertyManager;
import mj.skyconnect.rabies.android.R;
import mj.skyconnect.rabies.android.logic.FormController;
import mj.skyconnect.rabies.android.logic.PropertyManager;

import java.util.Collection;
import java.util.List;

/**
 * Handles general preferences.
 */
public class PreferencesActivity extends PreferenceActivity {
    public static final String INTENT_KEY_ADMIN_MODE = "adminMode";

    private AdminSharedPreferences sharedPreferences;


    protected static final int IMAGE_CHOOSER = 0;

    public static String KEY_LAST_VERSION = "lastVersion";
    public static String KEY_FIRST_RUN = "firstRun";
    public static String KEY_SHOW_SPLASH = "showSplash";
    public static String KEY_SPLASH_PATH = "splashPath";
    public static String KEY_FONT_SIZE = "font_size";
    public static String KEY_SELECTED_GOOGLE_ACCOUNT = "selected_google_account";
    public static String KEY_GOOGLE_SUBMISSION = "google_submission_id";

    public static String KEY_SERVER_URL = "server_url";
    public static String KEY_USERNAME = "username";
    public static String KEY_PASSWORD = "password";

    public static String KEY_PROTOCOL = "protocol";
    public static String KEY_FORMLIST_URL = "formlist_url";
    public static String KEY_SUBMISSION_URL = "submission_url";

    public static String KEY_COMPLETED_DEFAULT = "default_completed";

    public static String KEY_SHOW_START_SCREEN = "odk_show_entry_screen";
    public static String KEY_HELP_MODE_TRAY = "help_mode_tray";
    public static String KEY_PROGRESS_BAR = "progress_bar";
    public static String KEY_NAVIGATION_BAR = "pref_nav_bar";

    @Override
    public void onBuildHeaders(List<Header> target) {
        super.onBuildHeaders(target);

        sharedPreferences = AdminSharedPreferences.getInstance();

        final boolean adminMode = getIntent().getBooleanExtra(INTENT_KEY_ADMIN_MODE, false);

        if (adminMode) {
            loadHeadersFromResource(R.xml.general_preference_headers, target);
        } else {

            if (hasAtleastOneSettingEnabled(AdminKeys.serverKeys)) {
                loadHeadersFromResource(R.xml.server_preference_headers, target);
            }

            if (hasAtleastOneSettingEnabled(AdminKeys.userInterfaceKeys)) {
                loadHeadersFromResource(R.xml.user_interface_preference_headers, target);
            }

            if (hasAtleastOneSettingEnabled(AdminKeys.formManagementKeys)) {
                loadHeadersFromResource(R.xml.form_management_preference_headers, target);
            }

            if (hasAtleastOneSettingEnabled(AdminKeys.identityKeys)) {
                loadHeadersFromResource(R.xml.user_device_identity_preference_header, target);
            }
        }
    }

    @Override
    public void onHeaderClick(Header header, int position) {
        final boolean adminMode = getIntent().getBooleanExtra(INTENT_KEY_ADMIN_MODE, false);

        if (adminMode) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(INTENT_KEY_ADMIN_MODE, true);
            header.fragmentArguments = bundle;
        }

        super.onHeaderClick(header, position);
    }

    private boolean hasAtleastOneSettingEnabled(Collection<String> keys) {
        for (String key : keys) {
            boolean value = (boolean) sharedPreferences.get(key);
            if (value) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ViewGroup root = getRootView();
        Toolbar toolbar = (Toolbar) View.inflate(this, R.layout.toolbar, null);
        toolbar.setTitle(R.string.general_preferences);
        toolbar.setTitleTextColor(Color.WHITE);
        View shadow = View.inflate(this, R.layout.toolbar_action_bar_shadow, null);

        root.addView(toolbar, 0);
        root.addView(shadow, 1);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // the property manager should be re-assigned, as properties
        // may have changed.
        IPropertyManager mgr = new PropertyManager(this);
        FormController.initializeJavaRosa(mgr);
    }

    private ViewGroup getRootView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return (ViewGroup) findViewById(android.R.id.list).getParent().getParent().getParent();
        } else {
            return (ViewGroup) findViewById(android.R.id.list).getParent();
        }
    }
}
