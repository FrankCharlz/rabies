
package mj.skyconnect.rabies.android.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class FormReviewerDataModel {

    @SerializedName("case_id")
    @Expose
    public String caseId;

    @SerializedName("creator_id")
    @Expose
    public String creatorId;

    @SerializedName("fdata")
    @Expose
    public String fdata;

    @SerializedName("fdate")
    @Expose
    public String fdate;

    @SerializedName("rdata")
    @Expose
    public String rdata;

    @SerializedName("reviewer_id")
    @Expose
    public String reviewerId;

    @SerializedName("rdate")
    @Expose
    public String rdate;

}
