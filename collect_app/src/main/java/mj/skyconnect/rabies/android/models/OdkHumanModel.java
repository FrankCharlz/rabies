package mj.skyconnect.rabies.android.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OdkHumanModel implements Serializable {

    @SerializedName("sender")
    @Expose
    public User sender;

    @SerializedName("_URI")
    @Expose
    public String uRI;

    @SerializedName("_CREATOR_URI_USER")
    @Expose
    public String cREATORURIUSER;

    @SerializedName("_CREATION_DATE")
    @Expose
    public String cREATIONDATE;

    @SerializedName("_LAST_UPDATE_URI_USER")
    @Expose
    public Object lASTUPDATEURIUSER;

    @SerializedName("_LAST_UPDATE_DATE")
    @Expose
    public String lASTUPDATEDATE;

    @SerializedName("_MODEL_VERSION")
    @Expose
    public Object mODELVERSION;

    @SerializedName("_UI_VERSION")
    @Expose
    public Object uIVERSION;

    @SerializedName("_IS_COMPLETE")
    @Expose
    public String iSCOMPLETE;

    @SerializedName("_SUBMISSION_DATE")
    @Expose
    public String sUBMISSIONDATE;

    @SerializedName("_MARKED_AS_COMPLETE_DATE")
    @Expose
    public String mARKEDASCOMPLETEDATE;

    @SerializedName("GROUP_FIRST_VISIT_DATE_REPORTED_TO_HOSPITAL")
    @Expose
    public String gROUPFIRSTVISITDATEREPORTEDTOHOSPITAL;

    @SerializedName("GROUP_FIRST_VISIT_OWNER_NAME")
    @Expose
    public String gROUPFIRSTVISITOWNERNAME;

    @SerializedName("GROUP_FIRST_VISIT_OWNER_RESIDENCE")
    @Expose
    public String gROUPFIRSTVISITOWNERRESIDENCE;

    @SerializedName("LOCATION_OF_EVENT_REGION")
    @Expose
    public Object lOCATIONOFEVENTREGION;

    @SerializedName("GROUP_FIRST_VISIT_ANIMAL_ALIVE")
    @Expose
    public String gROUPFIRSTVISITANIMALALIVE;

    @SerializedName("PEP_AVAILABLE")
    @Expose
    public String pEPAVAILABLE;

    @SerializedName("LOCATION_OF_EVENT_WARD")
    @Expose
    public Object lOCATIONOFEVENTWARD;

    @SerializedName("SEX")
    @Expose
    public String sEX;

    @SerializedName("PEP_RECOMMENDED")
    @Expose
    public String pEPRECOMMENDED;

    @SerializedName("COMMENTS")
    @Expose
    public Object cOMMENTS;

    @SerializedName("VISIT_DATE")
    @Expose
    public String vISITDATE;

    @SerializedName("PHONE_NO")
    @Expose
    public String pHONENO;

    @SerializedName("POPULAR_NAME")
    @Expose
    public String pOPULARNAME;

    @SerializedName("WHOSE_PHONE")
    @Expose
    public String wHOSEPHONE;

    @SerializedName("LOCATION_OF_EVENT_DISTRICT")
    @Expose
    public Object lOCATIONOFEVENTDISTRICT;

    @SerializedName("META_INSTANCE_ID")
    @Expose
    public String mETAINSTANCEID;

    @SerializedName("GROUP_FIRST_VISIT_ASSESSMENT_DECISION")
    @Expose
    public String gROUPFIRSTVISITASSESSMENTDECISION;

    @SerializedName("LOCATION_OF_EVENT_VILLAGE")
    @Expose
    public Object lOCATIONOFEVENTVILLAGE;

    @SerializedName("HOW_USED")
    @Expose
    public String hOWUSED;

    @SerializedName("VILLAGE_IF_NOT_LISTED")
    @Expose
    public String vILLAGEIFNOTLISTED;

    @SerializedName("USERNAME")
    @Expose
    public String uSERNAME;

    @SerializedName("GROUP_FIRST_VISIT_WILDLIFE_SPECIFY")
    @Expose
    public Object gROUPFIRSTVISITWILDLIFESPECIFY;

    @SerializedName("GROUP_FIRST_VISIT_BITE_DETAILS")
    @Expose
    public String gROUPFIRSTVISITBITEDETAILS;

    @SerializedName("VICTIM_WAS_REFERED")
    @Expose
    public Object vICTIMWASREFERED;

    @SerializedName("GROUP_FIRST_VISIT_DATE_BITTEN")
    @Expose
    public String gROUPFIRSTVISITDATEBITTEN;

    @SerializedName("GROUP_FIRST_VISIT_ANIMAL_KNOWN")
    @Expose
    public String gROUPFIRSTVISITANIMALKNOWN;

    @SerializedName("AGE")
    @Expose
    public String aGE;

    @SerializedName("SURNAME")
    @Expose
    public String sURNAME;

    @SerializedName("HUMAN_ID")
    @Expose
    public Object hUMANID;

    @SerializedName("VICTIM_REFERED_TO")
    @Expose
    public Object vICTIMREFEREDTO;

    @SerializedName("VISIT_STATUS")
    @Expose
    public String vISITSTATUS;

    @SerializedName("OTHER_NAMES")
    @Expose
    public String oTHERNAMES;

    @SerializedName("GROUP_FIRST_VISIT_DP_INFORMED")
    @Expose
    public String gROUPFIRSTVISITDPINFORMED;

    @SerializedName("HAS_PHONE")
    @Expose
    public String hASPHONE;

    @SerializedName("GROUP_FIRST_VISIT_BITING_ANIMAL")
    @Expose
    public String gROUPFIRSTVISITBITINGANIMAL;
    private final static long serialVersionUID = 6813858777640413363L;

}