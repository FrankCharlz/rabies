package mj.skyconnect.rabies.android.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.list_item_cases.view.*
import mj.skyconnect.rabies.android.R
import mj.skyconnect.rabies.android.models.OdkHumanSimpleModel


/**
 * Created by Frank on 04-Dec-18.
 */
class CasesListAdapter(var items: List<OdkHumanSimpleModel>,
                       val mListener: IPositionClickListener) : RecyclerView.Adapter<CasesListAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return items.size;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_cases, parent, false));
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val currentItem = items[position]

        holder.tvTitle.text = currentItem.title
        holder.tvLeft.text = currentItem.date
        holder.tvRight.text = currentItem.sender

        holder.container.setOnClickListener {
            mListener.clicked(currentItem);
        }

    }


    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle = view.uid;
        val tvLeft = view._left;
        val tvRight = view._right;
        val container = view.container;

    }

    fun refreshData(positions: List<OdkHumanSimpleModel>) {
        items = positions
        notifyDataSetChanged()
    }

}