package mj.skyconnect.rabies.android.adapters;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import mj.skyconnect.rabies.android.FontManager;
import mj.skyconnect.rabies.android.R;


/**
 * Created by: Wickerman
 * Project title: cervical-android-app
 * Time: 11:32 AM
 * Date: 3/18/2017
 * Website: https://www.github.com/wickerlabs
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.SubmenuViewHolder> {

    private final String[] icons;
    private String[] items;
    private ClickCallback callback;

    public RecyclerAdapter(String[] items, String[] icons, ClickCallback clickCallback) {
        this.items = items;
        this.icons = icons;
        this.callback = clickCallback;
    }

    @Override
    public SubmenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mj.skyconnect.rabies.android.R.layout.submenu_card, parent, false);
        return new SubmenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SubmenuViewHolder holder, int position) {
        holder.submenuText.setText(items[position]);
        holder.submenuIcon.setText(icons[position]);
       FontManager.setTypeface(holder.submenuIcon, FontManager.ION_ICONS);
    }

    @Override
    public int getItemCount() {
        return items.length;
    }

    public interface ClickCallback {
        void onItemClick(int position);
    }

    class SubmenuViewHolder extends RecyclerView.ViewHolder {
        TextView submenuIcon;
        TextView submenuText;

        SubmenuViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(getLayoutPosition());
                }
            });


            submenuIcon =  itemView.findViewById(R.id.tv_icon);
            submenuText = itemView.findViewById(R.id.itemTextView);

        }
    }


}
