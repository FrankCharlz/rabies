package mj.skyconnect.rabies.android.activities2

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity

import java.util.HashMap

class IconTextView : android.support.v7.widget.AppCompatTextView {
    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }


    private fun init(context: Context) {
        gravity = Gravity.CENTER
        typeface = makeTypeface(context)
    }

    companion object {

        private val DEFAULT = "ionicons.ttf"
        private val cachedFonts = HashMap<String, Typeface>()

        private fun makeTypeface(context: Context, font: String = DEFAULT): Typeface? {

            if (!cachedFonts.containsKey(font)) {
                val tf = Typeface.createFromAsset(context.assets, font)
                cachedFonts[font] = tf
            }

            Log.i("font", cachedFonts[font].toString())

            return cachedFonts[font]
        }
    }

}
