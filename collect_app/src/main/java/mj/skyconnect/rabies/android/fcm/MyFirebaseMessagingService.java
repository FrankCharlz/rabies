/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mj.skyconnect.rabies.android.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import mj.skyconnect.rabies.android.R;
import mj.skyconnect.rabies.android.activities.MainMenu2Activity;
import mj.skyconnect.rabies.android.activities2.DataPointDetailsActivity;
import mj.skyconnect.rabies.android.application.Collect;
import mj.skyconnect.rabies.android.models.User;

import java.util.Map;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    /**
     * Called when message is received.
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Collect.log("From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() <= 0) {
            return; //message has no data
        }

        Map<String, String> message_data = remoteMessage.getData();
        String message_data_json = Collect.getGson().toJson(message_data);
        Collect.log("Message data json" + message_data_json);

        if (!User.isLoggedIn()) {
            return; //user not logged in
        }

        String category = message_data.get("category"); //data is lyk json, so treat it like json
        Collect.log( "category: " + category);

        final Intent intent;
        switch (category.toLowerCase()) {
            case "hw" :
                intent = new Intent(this, DataPointDetailsActivity.class);
                intent.putExtra(DataPointDetailsActivity.Companion.get_URI(), message_data.get("uri"));

                sendNotification(
                        "SES App",
                        "CHW has just uploaded case no: "+message_data.get("case_id"),
                        intent
                );
                break;

            case "lfo":
                intent = new Intent(this, MainMenu2Activity.class);
                sendNotification(
                        "SES App",
                        "VET has just uploaded case no: "+message_data.get("case_id"),
                        intent
                );
                break;

            default: break;
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     * @param message FCM message body received.
     */
    private void sendNotification(String title, String message, Intent intent) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(new Random().nextInt(0xfaded)/* ID of notification */, notificationBuilder.build());

    }
}