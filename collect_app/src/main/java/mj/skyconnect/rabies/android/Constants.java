package mj.skyconnect.rabies.android;

/**
 * Created by Frank on 29-Mar-18.
 */

public class Constants {

    public static final String BASE_URL_V4 = "http://rabies.esurveillance.or.tz/api/";

    public static final String PREFS_SENT_GCM_TOKEN = "__sent_token" ;
    public static final String PREFS_HELP_DESK_NO = "__help_desk_phone_no";
    public static final String PREFS_HELP_DESK_EMAIL = "__email_shit";
    public static final String PREFS_LANG = "__pref_language";

    public static final String INSTANCES_AUTHORITY = "mj.skyconnect.rabies.android.provider.odk.instances";
    public static final String FORMS_AUTHORITY = "mj.skyconnect.rabies.android.provider.odk.forms";


}
