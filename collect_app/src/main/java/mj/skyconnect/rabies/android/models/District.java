package mj.skyconnect.rabies.android.models;

import java.util.List;

/**
 * Created by Frank on 27-Jun-18.
 */

public class District {
    public int id, region;
    public String name;
    public List<Facility> facilities;
}
