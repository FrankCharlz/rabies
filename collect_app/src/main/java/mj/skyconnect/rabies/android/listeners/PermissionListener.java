package mj.skyconnect.rabies.android.listeners;

/**
 * Created by Frank on 29-Jun-18.
 */

public interface PermissionListener {

    void granted();

    void denied();
}
