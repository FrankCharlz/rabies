package mj.skyconnect.rabies.android.externalintents;

import junit.framework.Assert;

import java.io.File;

import mj.skyconnect.rabies.android.application.Collect;

class ExportedActivitiesUtils {

    private static final String[] DIRS = new String[]{
            Collect.ODK_ROOT, Collect.FORMS_PATH, Collect.INSTANCES_PATH, Collect.CACHE_PATH, Collect.METADATA_PATH, Collect.OFFLINE_LAYERS
    };

    static void clearDirectories() {
        for (String dirName : DIRS) {
            File dir = new File(dirName);
            if (dir.exists()) {
                if (dir.delete()) {
                    System.out.println("Directory was not deleted");
                }
            }
        }

    }

    static void testDirectories() {
        for (String dirName : DIRS) {
            File dir = new File(dirName);
            Assert.assertTrue("File " + dirName + "does not exist", dir.exists());
            Assert.assertTrue("File" + dirName + "does not exist", dir.isDirectory());
        }
    }

}
